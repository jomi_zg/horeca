=== Badge Management for WooCommerce ===

Contributors: phoeniixx
URL: http://phoeniixx.com/
Tags: badges, badge, woocommerce, products, e-commerce, shop, badges management, product badge, global badge, category badge, woocommerce badge , Phoeniixx, Badge Management for WooCommerce
Requires at least: 4.0
Tested up to: 4.6
Stable tag: 1.2.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin allows you to add badges to products on your ecommerce site. Badges on a product help you highlight special offers of the products. 

== Description ==


Customers are naturally drawn to see highlighted information when visiting your site. Badge Management Plugin allows you to highlight 'special' features and 'new' offers on your products, so as to catch your customers' attention. With this plugin you could customize a badge's text & its color and could also set its dimension and position. The plugin provides you with some default image-badges as well.

= Main features =

1. Option to have Text Badge or Image Badge
2. 4 Default Image-Badge options available to choose from 
3. Option to customize text badges
4. Option to Set Text Options ( Color, Font Size and Line Height)
5. Option to Set Badge Position
6. Option to Set Badge Dimensions 
7. Option to Show or Hide 'On Sale' Badge, as per your choice

<blockquote>
<h4>13 Premium Features</h4>
<ol>
<li> Option to Add a particular Badge to recent products</li>
<li> Option to Add a particular Badge to ‘On Sale’ products</li>
<li> Option to Add a specific Badge to featured products</li>
<li> Option to Hide the default WooCommerce ‘On Sale’ Badge</li>
<li> Option to Hide Badge in single Product Page</li>
<li> Option to Add a particular Badge to each Product Category</li>
<li> 9 Badge images available</li>
<li> Option to Upload your own images for Badges</li>
<li> Option to Set Badge Opacity</li>
<li> Option to Set Badge Position</li>
<li> Options (Border Radius, Padding, Opacity) for defining Badge Container</li>
<li> Option to Set Expiry Date for Badges</li>
<li> Option to Set Expiry Date & Time on any of the badges</li>
</ol>
<li> <a href="http://phoeniixx.com/demo/woocommerce/badge-management/">View Live Demo</a> | <a href="http://www.phoeniixx.com/product/badge-management-for-woocommerce/">Buy Premium Plugin</a></li>
</ul>
</blockquote>

<blockquote>
<h4>Our Premium Themes</h4>
<ul>
<li> Jstore Theme - <a href="https://phoeniixx.com/demo/themes/jstore/">View Live Demo</a> | <a href="https://www.phoeniixx.com/product/jstore-theme/">Buy Premium Theme</a></li>
</ul>
<p></p>
</blockquote>

**Support Email :** - [support@phoeniixx.com](mailto:support@phoeniixx.com)

**Website** - [http://www.phoeniixx.com/products/](http://www.phoeniixx.com/products/)


== Installation ==

1. Install [WordPress](https://wordpress.org/)
2. Go to your WordPress admin panel, and then to Plugins. Click on "Add New" and then upload the zip file of the plugin using the "Upload Plugin" button you can find on top of the screen.
3. For the plugin to work as it should, WooCommerce plugin has to be installed and enabled.


== Frequently Asked Questions ==

** Coming Soon


== Screenshots ==

1. It shows how Badges will be shown on Product page.
2. It shows how Badges will be shown on Shop page.
3. It shows the created Badges on Product page.
4. It shows the Admin Settings page. 
5. It shows the list of created badges.
6. It Shows the Add new badge page in backend when we create Text badge.
7. It Shows the Add new badge page in backend when we create Image badge.


== Upgrade notice ==

= 1.2.3 - 28/06/2016 =
* Added new images 

= 1.2.2 - 18/06/2016 =
* Bug fixes

= 1.2.1 - 19/04/2016 =
* Bug fixes

= 1.2 - 8/12/2015 =
* Css fixes

= 1.1 - 27/11/2015 =
* Css fixes

= 1.0 =
Initial release
