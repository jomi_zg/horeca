;(function($) {

	$('#_upcoming').on('change', function() { 
	    // From the other examples
	    if (!this.checked) {
	        $(this).closest('._upcoming_field').siblings('._available_on_field').hide();
	    } else {
	    	$(this).closest('._upcoming_field').siblings('._available_on_field').show();
	    }
	});

	$('#_available_on').datepicker();

})(jQuery);