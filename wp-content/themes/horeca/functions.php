<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//  
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}
add_filter( 'woocommerce_product_tabs', 'woo_edit_tabs' );
function woo_edit_tabs( $tabs ) {
	
	// Adds the new tab
	
	$tabs['enquiry_form'] = array(
		'title' 	=> __( 'Zatraži ponudu za ovaj proizvod', 'woocommerce' ),
		'priority' 	=> 50,
		'callback' 	=> 'woo_enquiry_form'
	);

	return $tabs;

}
function woo_enquiry_form() {
	echo '<h2>' . __( 'Zatraži ponudu', 'woocommerce' ) . '</h2>';
	echo '<p>' . __( 'Zainteresirani ste za ovaj proizvod? Pošaljite nam upit. Ispunite formular ispod.', 'woocommerce' ) . '</p>';
	echo do_shortcode( '[ninja_forms id=8]' );
	
}
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

//
// Your code goes below
//