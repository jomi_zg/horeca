<?php
/**
 * The template part for displaying a message that posts cannot be found.
 */
?>
<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Nije pronađeno!', 'zerif-lite' ); ?></h1>
	</header><!-- .page-header -->
	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
		
			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'zerif-lite' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
			
		<?php elseif ( is_search() ) : ?>
			<p><?php _e( 'Vaša pretraga nije polučila rezultate. Pokušajte ponovno.', 'zerif-lite' ); ?></p>
			<?php get_search_form(); ?>
		<?php else : ?>
			<p><?php _e( 'Ne možemo  pronaći &rsquo; &rsquo;. Probajte pretražiti pojam.', 'zerif-lite' ); ?></p>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->